// Copyright 2018 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

'use strict';
chrome.runtime.onMessage.addListener(function(request, sender, sendResponse){
    if(request.action == 'reloadPage'){
        chrome.tabs.reload(function(){});
        return Promise.delay(4000).then(function(){
            chrome.tabs.query({active: true, currentWindow:true}, function(tabs){
                chrome.tabs.sendMessage(tabs[0].id, {action: "continue", statusNumber: request.statusNumber, likedNumber: request.likedNumber})
            })
        });

    }
})
