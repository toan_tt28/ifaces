// Copyright 2018 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

'use strict';
$(function(){


    chrome.storage.sync.get(['likedNumber'], function(iFaces){
        $('#count').html(iFaces.likedNumber);
    })
    $('#buttonStart').click(function(){

        var statusNumber = parseInt($('input[name="statusNumber"]').val());
        console.log(statusNumber);
        if(Number.isInteger(statusNumber)){
            //console.log('vaod sendeve');
            chrome.tabs.query({active: true, currentWindow:  true}, function(tabs){
                chrome.storage.sync.set({likedNumber: 0},function(){
                    $('#count').html(0);
                })
                chrome.tabs.sendMessage(tabs[0].id, {action: "start", statusNumber: statusNumber});
            });
        }else{
            alert("Nhập số đi mấy má!!");
        }


    })
})
